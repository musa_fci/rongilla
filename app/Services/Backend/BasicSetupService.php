<?php

namespace App\Services\Backend;

use DB;
use App\Http\Helper;


class BasicSetupService
{
      //*******************//
     //*******Category****//
    //*******************//
	public static function saveCategory($data = null)
	{
		try {
			DB::beginTransaction();
			$catId = DB::table('categories')
					->insertGetId([
						'category_name_en' => $data['category_name_en'],
						'category_name_bn' => $data['category_name_bn'],
						'is_selected'	   => $data['is_selected']
					]);
					
			if($catId){
				$icon    = Helper::imageUpload($catId, $data['icon'], '/images/category/icon/', 50, 50);
				$feature = Helper::imageUpload($catId, $data['feature_img'], '/images/category/feature/', 300, 500);
				$status  = DB::table('categories')
							->where('id',$catId)
							->update([
								'icon'			=>	$icon,
								'feature_img'	=>	$feature
							]);
			}
			DB::commit();
			return true;

		} catch (\Exception $e) {
			return $e;
		}
	}

      //*******************//
     //***Sub-Category****//
    //*******************//
    public static function getSubCategory()
	{
		$result = DB::table('sub_categories')
				  ->join('categories','sub_categories.fk_category_id','=','categories.id')
				  ->orderBy('sub_categories.id','DESC')
				  ->get(['sub_categories.*','categories.category_name_en']);
		return $result;
	}

	public static function saveSubCategory($data = null)
	{
		try {
			DB::beginTransaction();
			$subCatId = DB::table('sub_categories')
						->insertGetId([
							'fk_category_id' 	   =>	$data['fk_category_id'],
							'sub_category_name_en' =>	$data['sub_category_name_en'],
							'sub_category_name_bn' =>	$data['sub_category_name_bn']
						]);

			if($subCatId){
				$feature = Helper::imageUpload($subCatId, $data['feature_img'], '/images/subCategory/feature/', 300, 500);
				$status  = DB::table('sub_categories')
						   ->where('id',$subCatId)
						   ->update([
						   		'feature_img'	=>	$feature
						   ]);
			}
			DB::commit();
			return true;
			
		} catch (\Exception $e) {
			return $e;
		}
	}



	  //*******************//
     //*Sub-Sub-Category**//
    //*******************//
    public static function getSubSubCategory()
    {
    	$result = DB::table('sub_sub_categories as ssc')
    			  ->join('categories as c','ssc.fk_category_id','=','c.id')
    			  ->join('sub_categories as sc','ssc.fk_sub_category_id','=','sc.id')
    			  ->orderBy('ssc.id','DESC')
    			  ->get([
    			  	'ssc.*',
    			  	'sc.sub_category_name_en',
    			  	'c.category_name_en'
    			  ]);
    	return $result;
    }


    public static function saveSubSubCategory($data = null)
    {
    	try {
    		DB::beginTransaction();
    		$subSubCat = DB::table('sub_sub_categories')
    					->insertGetId([
    						'fk_category_id'			=>	$data['fk_category_id'],
    						'fk_sub_category_id'		=>	$data['fk_sub_category_id'],
    						'sub_sub_category_name_en'	=>	$data['sub_sub_category_name_en'],
    						'sub_sub_category_name_bn'	=>	$data['sub_sub_category_name_bn'],
    					]);

    		if($subSubCat){
    			$feature = Helper::imageUpload($subSubCat, $data['feature_img'], '/images/subSubCategory/feature/', 300, 500);
    			$status  = DB::table('sub_sub_categories')
    					   ->where('id',$subSubCat)
    					   ->update([
    					   		'feature_img'	=>	$feature
    					   ]);
    		}
    		DB::commit();
    		return true;

    	} catch (\Exception $e) {
    		return $e;
    	}
    }


      //*******************//
     //*******Brand*******//
    //*******************//
    public static function saveBrand($data = null)
    {
    	try {
    		DB::beginTransaction();
	    	$brandId = DB::table('brands')
		   			   ->insertGetId([
					   		'brand_name_en'	=>	$data['brand_name_en'],
					   		'brand_name_bn'	=>	$data['brand_name_bn'],
					   		'brand_link'	=>	$data['brand_link'],
					   ]);

			if($brandId){
				$feature = Helper::imageUpload($brandId, $data['feature_img'], '/images/brand/feature/', 300, 500);
				$status = DB::table('brands')
						  ->where('id',$brandId)
						  ->update([
						  		'feature_img'	=>	$feature
						  ]);
			}    		

    		DB::commit();
    		return true;

    	} catch (\Exception $e) {
    		return $e;
    	}
    }


      //*******************//
     //*********Tag*******//
    //*******************//
    public static function saveTag($data = null)
    {
    	try {
    		DB::beginTransaction();
	    	DB::table('tags')
   			   ->insert([
			   		'tag_name_en'	=>	$data['tag_name_en'],
			   		'tag_name_bn'	=>	$data['tag_name_bn'],
			   ]);

    		DB::commit();
    		return true;

    	} catch (\Exception $e) {
    		return $e;
    	}
    }


      //*******************//
     //*********Color*****//
    //*******************//
    public static function saveColor($data = null)
    {
    	try {
    		DB::beginTransaction();
    		DB::table('colors')
			   ->insert([
			   		'color_name_en'	=>	$data['color_name_en'],
			   		'color_name_bn'	=>	$data['color_name_bn'],
			   		'color_code'	=>	$data['color_code'],
			   ]);

			DB::commit();
			return true;
    		
    	} catch (\Exception $e) {
    		return $e;
    	}
    }


    //*******************//
    //*********Tag*******//
    //*******************//
    public static function saveSize($data = null)
    {
        try {
            DB::beginTransaction();
            DB::table('sizes')
               ->insert([
                    'size_name_en'   =>  $data['size_name_en'],
                    'size_name_bn'   =>  $data['size_name_bn'],
               ]);

            DB::commit();
            return true;

        } catch (\Exception $e) {
            return $e;
        }
    }







}