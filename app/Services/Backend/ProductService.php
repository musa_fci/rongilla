<?php

namespace App\Services\Backend;

use DB;
use App\Http\Helper;

class ProductService
{
	  //******************//
     //***Product********//
    //******************//

	public static function saveProduct($sellerId,$data = null)
	{
		$productCode = DB::table('products')->max('product_code');
        $productCode = ((is_null($productCode) || $productCode == 0) ? 100+$productCode+1 : $productCode+1);

		$price_after_discount = $data['price'] - ($data['price'] * $data['discount'])/100;
		$commission = ($price_after_discount * $data['commission'])/100;
		try {
			DB::beginTransaction();
			$productId = DB::table('products')
			 ->insertGetId([
			 	'fk_seller_id'			 =>	$sellerId,
			 	'fk_category_id'		 =>	$data['fk_category_id'],
			 	'fk_sub_category_id'	 =>	$data['fk_sub_category_id'],
			 	'fk_sub_sub_category_id' =>	$data['fk_sub_sub_category_id'],
			 	'product_name_en'		 =>	$data['product_name_en'],
			 	'product_name_bn'		 =>	$data['product_name_bn'],
			 	'slug'					 =>	$data['slug'],
			 	'product_code'			 =>	$productCode,
			 	'details_en'			 =>	$data['details_en'],
			 	'details_bn'			 =>	$data['details_bn'],
			 	'product_video'			 =>	$data['product_video'],
			 	'quantity'				 =>	$data['quantity'],
			 	'product_type'			 =>	$data['product_type'],
			 	'price'					 =>	$data['price'],
			 	'discount'	 			 =>	$data['discount'],
			 	'price_after_discount'	 => $price_after_discount,
			 	'commission'			 =>	$commission,
			 	'placement_type'		 =>	$data['placement_type'],
			 	'refund_policy'			 =>	$data['refund_policy'],
			 	'comment'				 =>	$data['comment']
			 ]);

			if($productId){
				for($i=0; $i<count($data['color_id']); $i++){
					DB::table('product_wise_color')
					->insert([
						'fk_seller_id'   =>	$sellerId,
						'fk_product_id'  =>	$productId,
						'fk_color_id'    =>	$data['color_id'][$i],
					]);
				}

				for($i=0; $i<count($data['size_id']); $i++){
					DB::table('product_wise_size')
					->insert([
						'fk_seller_id'	=> $sellerId,
						'fk_product_id'	=> $productId,
						'fk_size_id'	=> $data['size_id'][$i],
					]);
				}

				for($i=0; $i<count($data['tag_id']); $i++){
					DB::table('product_wise_tag')
					->insert([
						'fk_seller_id'	=> $sellerId,
						'fk_product_id'	=> $productId,
						'fk_tag_id'		=> $data['tag_id'][$i],
					]);
				}				
			}


			if($productId){
				if (isset($data['image'])) {
					for($i=0; $i<count($data['image']); $i++){
						$imageType = ($i == 0)? 1 : 2;
						$imageCaption = $data['image'][$i]->getClientOriginalName();
						$image = Helper::imageUpload($productId.'-'.$i, $data['image'][$i], '/images/products/', 700, 700);
					    DB::table('product_wise_image')
					    ->insert([
					   		'fk_seller_id'	=>  $sellerId,
							'fk_product_id'	=>  $productId,
							'caption'		=>  $imageCaption,
							'type'			=>  $imageType,
					   		'image_path'    =>	$image
					    ]);
					}
				}
			}



			DB::commit();
			return true;
			
		} catch (Exception $e) {
			echo $e;
		}
	}
}