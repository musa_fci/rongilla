<?php

namespace App\Http;

use DB;

class CustomGlobal
{
	public static function getData($table = null)
	{
		$result = DB::table($table)
				      ->orderBy('id','DESC')
				      ->get();
		return $result;
	}

	public static function getActiveData($table = null)
	{
		$result = DB::table($table)
				      ->where('status',1)
				      ->get();
		return $result;
	}


	public static function getDataById($table = null, $id = null)
	{
		$result = DB::table($table)
				      ->where('id',$id)
				      ->first();
		return $result;
	}


	public static function deleteData($table = null, $id = null)
	{
		$result =  DB::table($table)
				   	   ->where('id','=',$id)
				       ->update(['status'=>0]);
		return $result;
	}


}