<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('SuperAdmin.id')){
            return back()->with('fail','You are not Login...!');
        }
        return $next($request);
    }
}
