<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color_name_en' =>  'required',
            'color_name_bn' =>  'required',
            'color_code' =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'color_name_en.required' =>  'Color Name (English) is Required',
            'color_name_bn.required' =>  'Color Name (Bangla) is Required',            
            'color_code.required'    =>  'Color Code is Required',            
        ];
    }
}
