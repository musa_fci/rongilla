<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fk_category_id'            =>  'not_in:0',
            'fk_sub_category_id'        =>  'not_in:0',
            'sub_sub_category_name_en'  =>  'required',
            'sub_sub_category_name_bn'  =>  'required',
            'feature_img'               =>  'required|mimes:jpeg,jpg,png|max:500',
        ];
    }

    public function messages()
    {
        return [
            'fk_category_id.not_in'             =>  'Must Select a Category Name',
            'fk_sub_category_id.not_in'         =>  'Must Select a Sub-Category Name',
            'sub_sub_category_name_en.required' =>  'Sub-Sub-Category Name (English) is Required',
            'sub_sub_category_name_bn.required' =>  'Sub-Sub-Category Name (Bangla) is Required',
            'feature_img.required'               => 'Feature Image is Required',
        ];
    }
}
