<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fk_category_id'            =>  'not_in:0',
            'fk_sub_category_id'        =>  'not_in:0',
            'fk_sub_sub_category_id'    =>  'not_in:0',
            'product_name_en'           =>  'required',
            'product_name_bn'           =>  'required',
            'slug'                      =>  'required|alpha_dash',
            'details_en'                =>  'required',
            'details_bn'                =>  'required',
            'quantity'                  =>  'required|numeric',
            'product_type'              =>  'not_in:0',
            'price'                     =>  'required|numeric',
            'commission'                =>  'required|numeric|between:5,30',
            'placement_type'            =>  'not_in:0',
            'image'                     =>  'required',
            'refund_policy'             =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'fk_category_id.not_in'             =>  'Select Correct Category',
            'fk_sub_category_id.not_in'         =>  'Select Correct Sub-Category',
            'fk_sub_sub_category_id.not_in'     =>  'Select Correct Sub-Sub-Category',
            'product_type.not_in'               =>  'Select Correct Product Type',
            'placement_type.not_in'             =>  'Select Correct Delivery Place',            
        ];
    }
}


            // 'image.dimensions'                  =>  'Image size must be 700 x 700',
            // 'image.mimes'                       =>  'Image Type must be jpeg,jpg,png,jpg,gif,svg',
