<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tag_name_en' =>  'required',
            'tag_name_bn' =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'tag_name_en.required' =>  'Tag Name (English) is Required',
            'tag_name_bn.required' =>  'Tag Name (Bangla) is Required',            
        ];
    }
}
