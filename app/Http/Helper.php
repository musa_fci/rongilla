<?php

namespace App\Http;

use Image;

class Helper
{
	public static function imageUpload($id = null, $image = null, $path = null, $height = null, $width = null)
	{
      if($image->isValid())
        {
            $destination = public_path().$path;
            $extention = $image->getClientOriginalExtension();
            $imgName = $id.'-'.date('Ymd').'-'.time().'.'.$extention;

            if ($width && $height) {
                Image::make($image->getRealPath())
                    ->resize($width, $height)
                    ->save($destination . $imgName)
                    ->destroy();
            } else {
                Image::make($image->getRealPath())
                    ->save($destination . $imgName)
                    ->destroy();
            }
        }
        return $imgName;
	}

}