<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use DB;
use Auth;
use Hash;

class ClientController extends Controller
{


	//*******************
    //*******Client Login
    //*******************

	public function login()
	{
		return view('frontend.sign-in');
	}



	public function checkLogin(Request $request)
	{
		$request->validate([
			'email'		=>	'required',
			'password'	=>	'required'
		]);

		$credential = ['email' => $request->email,'password' => $request->password];

	    if(Auth::guard('client')->attempt($credential)){
	        session(['client' => Auth::guard('client')->user()]);
	        return "login success";
	    }else{	      	
	        return redirect('sign-in')->with('fail','Login Information Is Wrong...!');
	    }
	}


	public function clientLogout()
	{
		if(session()->has('client.id'))
		{
			session()->forget('client');
			return redirect('sign-in')->with('success','Logout Successfull');
		}
		return "<h1>You are not login yet !</h1>";     
	}


	//*******************
    //Client Registration 
    //*******************

    public function registration(Request $request)
    {
    	$request->validate([
    		'name'		=>	'required',
    		'email'		=>	'required|email|unique:clients',
    		'phone'		=>	'required|unique:clients',
    		'password'	=>	'required|confirmed',
    	]);

    	DB::table('clients')->insert([
    		'name'		=>		$request->name,
    		'email'		=>		$request->email,
    		'phone'		=>		$request->phone,
    		'password'	=>		Hash::make($request->password)
    	]);

    	return redirect()->back()->with('success','Your Account Create Successfully !');
    }


}
