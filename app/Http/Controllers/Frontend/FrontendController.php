<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class FrontendController extends Controller
{
    public function index()
    {
    	$products = DB::table('products')
    		->join('product_wise_image','products.id','=','product_wise_image.fk_product_id')
    		->where('products.status',1)
    		->where('product_wise_image.type',1)
    		->get([
    			'products.id',
    			'products.fk_seller_id',
    			'products.slug',
    			'products.product_code',
    			'products.product_name_en',
    			'products.product_type',
    			'products.price',
    			'products.price_after_discount',
    			'product_wise_image.image_path'
    		]);
    	return view('frontend.index',compact('products'));
    }


    public function singleProductDetail($id = null, $slug = null)
    {
        $product = DB::table('products')
            ->where('products.id',$id)
            ->where('products.slug',$slug)
            ->first([
                'products.id',
                'products.fk_seller_id',
                'products.product_name_en',
                'products.product_code',
                'products.details_en',
                'products.price',
                'products.price_after_discount',
            ]);
        $product->colors = $this->product_wise_colors($product->id);
        $product->sizes = $this->product_wise_sizes($product->id);
        // dd($product);
    	return view('frontend.detail',compact('product'));
    }

    private function product_wise_colors($productId)
    {
        return DB::table('product_wise_color as pwc')
            ->join('colors','pwc.fk_color_id','=','colors.id')
            ->where('pwc.fk_product_id',$productId)
            ->get([
                'pwc.id as product_wise_color_id',
                'colors.id as color_id',
                'colors.color_name_en',
                'colors.color_code',
            ]);
    }
    private function product_wise_sizes($productId)
    {
        return DB::table('product_wise_size as pws')
            ->join('sizes','pws.fk_size_id','=','sizes.id')
            ->where('pws.fk_product_id',$productId)
            ->get([
                'pws.id as product_wise_size_id',
                'sizes.id as size_id',
                'sizes.size_name_en',
            ]);
    }
}
