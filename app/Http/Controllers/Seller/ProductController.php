<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Http\CustomGlobal;
use App\Http\Requests\ProductRequest;
use App\Services\Backend\ProductService;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getSubCategory($categoryId)
    {
        return DB::table('sub_categories')
               ->where('fk_category_id',$categoryId)
               ->where('status',1)
               ->get();
    }

    public function getSubSubCategory($subCategoryId)
    {
        return DB::table('sub_sub_categories')
               ->where('fk_sub_category_id',$subCategoryId)
               ->where('status',1)
               ->get();
    }



    public function product()
    {
        $colors           = CustomGlobal::getActiveData('colors');
        $sizes            = CustomGlobal::getActiveData('sizes');
        $tags             = CustomGlobal::getActiveData('tags');
        $categories       = CustomGlobal::getActiveData('categories');    

    	return view('seller.product.addProduct',compact('categories','colors','sizes','tags'));
    }

    public function saveProduct(ProductRequest $request)
    {
        $sellerId = session()->get('SellerAdmin.id');        
        $saveProduct = ProductService::saveProduct($sellerId,$request->all());
        if($saveProduct){
            return redirect('product')->with('success','Product Save Successfully !');
        } else {
            return redirect('product')->with('fail','Something Went Wrong !');
        }    	
    }
}
