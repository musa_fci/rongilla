<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Mail\verifyEmail;
use DB;
use App\Seller;
use Mail;
use Hash;
use Auth;
use Session;



class SellerController extends Controller
{

    //*******************
    //Seller Login
    //*******************
    public function sellerLogin()
    {
        if(Session::has('SellerAdmin.id')){
            return redirect()->back();
        }
    	return view('seller.login');
    }



    public function checkSellerLogin(Request $request)
    {
        $request->validate([
            'email'     =>  'required',
            'password'  =>  'required', 
        ]);

        $data = DB::table('sellers')
                ->where('email',$request->email)
                ->first();
        // dd($data);

        if($data->email==$request->email AND Hash::check($request->password,$data->password)){
            Session::put('SellerAdmin',(array)$data);                     
            return redirect('seller-dashboard')->with('success','Login Successfully !');            
        }else{
            return back()->with('fail','Login Information not Match !');
        }
    }


    public function sellerLogout()
    {
        if(Session::has('SellerAdmin.id'))
        {
           Session::forget('SellerAdmin');
           return redirect('SellerAdmin')->with('success','Logout Successfull');
        }
        return "<h1>You are not login yet !</h1>";        
    }



    public function SellerUpdate(Request $request)
    {   
        $id =  session::get('SellerAdmin.id');        

        
        $data = DB::table('sellers')
                ->where('id',$id)
                ->update([
                    'name'              => $request->name,
                    'email'             => $request->email,
                    'mobile'            => $request->mobile,
                    'payment_method'    => $request->payment_method,
                    'ac_holder_name'    => $request->ac_holder_name,
                    'ac_number'         => $request->ac_number,
                    'business_name'     => $request->business_name,
                    'business_type'     => $request->business_type,
                    'website'           => $request->website,
                    'district'          => $request->district,
                    'address'           => $request->address
                ]);

                if(Session::has('SellerAdmin.id')){
                   Session::forget('SellerAdmin');                   
                }                
                return redirect('SellerAdmin')->with('success','Data Update Successfully, But You Need to login again..!');
    }



    //*******************
    //Seller Registration
    //*******************
    public function registraView()
    {
    	return view('seller.register');
    }

    public function registration(Request $request)
    {
    	$request->validate([
    		'name'		=> 'required',
    		'email'		=> 'required|email|unique:sellers',
    		'password'	=> 'required|confirmed|min:8|max:16',
    		'mobile'	=> 'required|unique:sellers',
    	]);

    	$seller = DB::table('sellers')->insert([
        		'name'		     => $request->name,
        		'email'		     => $request->email,
        		'password'	     => Hash::make($request->password),
        		'mobile'	     => $request->mobile,
                'verify_token'   => Str::random(40)
    	    ]);


        $thisSeller = DB::table('sellers')->get()->last();
        $thisSeller = ((array)$thisSeller);        
        $this->sendEmail($thisSeller);
        return "ok";  

    	// return redirect('SellerAdmin')->with('success','Registration Successfully !!');
    }


    public function sendEmail($thisSeller)
    {
        Mail::to($thisSeller['email'])->send(new verifyEmail($thisSeller));
    }


    // public function afterSellerVerify($email)
    // {
    //     return $email;
    // }


}
