<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use Session;

class SuperAdminController extends Controller
{
    public function index()
    {
        return view('backend.index');
    }

    public function login()
    {   
        if(Session::has('SuperAdmin.id')){
            return redirect()->back();
        }
    	return view('backend.login');
    }


    public function checkLogin(Request $request)
    {
    	$request->validate([
    		'email'		=>	'required',
    		'password'	=>	'required',	
    	]);

    	$data = DB::table('admins')->first();        

    	if($data->email==$request->email AND Hash::check($request->password,$data->password)){
    		Session::put('SuperAdmin',(array)$data);                     
            return redirect('admin')->with('success','Login Successfully !');            
    	}else{
    		return back()->with('fail','Login Information not Match !');
    	}
    }


    public function logout()
    {
        if(Session::has('SuperAdmin.id')){         
           Session::forget('SuperAdmin');
           return redirect('SuperAdmin')->with('success','Logout Successfull');
        }
        return "<h1>You are not login yet !</h1>";
    }


}
