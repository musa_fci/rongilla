<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\CustomGlobal;
use App\Http\Requests\BrandRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ColorRequest;
use App\Http\Requests\SizeRequest;
use App\Http\Requests\SubCategoryRequest;
use App\Http\Requests\SubSubCategoryRequest;
use App\Http\Requests\TagRequest;
use App\Services\Backend\BasicSetupService;
use DB;
use Illuminate\Http\Request;

class BasicSetupController extends Controller
{

    //*******************
    //*******Category****
    //*******************
	public function category()
	{
		$categories = CustomGlobal::getActiveData('categories');
		return view('backend.basicSetup.category',compact('categories'));
	}


	public function saveCategory(CategoryRequest $request)
	{
		$saveCategory = BasicSetupService::saveCategory($request->all());

		if($saveCategory){
			return redirect('category')->with('success','Category Save Successfully !');
		} else {
			return redirect('category')->with('fail','Something Went Wrong !');
		}
	}

/*    public function deleteCategory($id = null)
    {
        $result =  CustomGlobal::deleteData('categories',$id);

        if($result){
            return redirect('category')->with('success','Category Delete Successfully !');
        } else {
            return redirect('category')->with('fail','Something Went Wrong !');
        }
    }*/

    public function editCategory($id = null)
    {
        $result = CustomGlobal::getDataById('categories',$id);
        return view('backend.basicSetup.category',compact('result'));
        
    }


    //*******************
    //***Sub-Category****
    //*******************
	public function subCategory()
	{
		$categories = CustomGlobal::getActiveData('categories');
		$subCategories = BasicSetupService::getSubCategory();
		return view('backend.basicSetup.sub-category',compact('categories','subCategories'));
	}


	public function saveSubCategory(SubCategoryRequest $request)
	{
		$saveSubCategory = BasicSetupService::saveSubCategory($request->all());

		if($saveSubCategory === true){
			return redirect('sub-category')->with('success','Sub-Category Save Successfully !');
		} else {
			return redirect('sub-category')->with('fail','Something Went Wrong !');
		}
	}


	//*******************
    //*Sub-Sub-Category**
    //*******************
	public function subSubCategory()
	{
		$categories     = CustomGlobal::getActiveData('categories');
		$subCategories  = CustomGlobal::getActiveData('sub_categories');
		$subSubCategory = BasicSetupService::getSubSubCategory();
		return view('backend.basicSetup.sub-sub-category',compact('categories','subCategories','subSubCategory'));
	}

    public function getSubCategory($categoryId)
    {
        return DB::table('sub_categories')
            ->where('fk_category_id',$categoryId)
            ->where('status',1)
            ->get();
    } 

	public function saveSubSubCategory(SubSubCategoryRequest $request)
	{
		$saveSubSubCategory = BasicSetupService::saveSubSubCategory($request->all());

		if($saveSubSubCategory === true){
			return redirect('sub-sub-category')->with('success','Sub-Sub-Category Save Successfully !');
		} else {
			return redirect('sub-sub-category')->with('fail','Something Went Wrong !');
		}
	}


	  //*******************
     //********Brand******
    //*******************

    public function brand()
    {
    	$brands = CustomGlobal::getData('brands');
    	return view('backend.basicSetup.brand',compact('brands'));
    }

    public function saveBrand(BrandRequest $request)
    {
    	$brandSave = BasicSetupService::saveBrand($request->all());

    	if($brandSave === true){
    		return redirect('brand')->with('success','Brand Save Successfully !');
    	} else {
    		return redirect('brand')->with('fail','Something Went Wrong !');
    	}
    }

      //*******************
     //********Tag********
    //*******************
    public function tag()
    {
    	$tags = CustomGlobal::getData('tags');
    	return view('backend.basicSetup.tag',compact('tags'));
    }


    public function saveTag(TagRequest $request)
    {
    	$saveTag = BasicSetupService::saveTag($request->all());

    	if($saveTag === true){
    		return redirect('tag')->with('success','Tag Save Successfully !');
    	} else {
    		return redirect('tag')->with('fail','Something Went Wrong !');
    	}
    }


      //*******************
     //********Color******
    //*******************
    public function color()
    {
    	$colors = CustomGlobal::getData('colors');
    	return view('backend.basicSetup.color',compact('colors'));
    }


    public function saveColor(ColorRequest $request)
    {    	
    	$saveColor = BasicSetupService::saveColor($request->all());

    	if($saveColor === true){
    		return redirect('color')->with('success','Color Save Successfully !');
    	} else {
    		return redirect('color')->with('fail','Something Went Wrong !');
    	}
    }


    //*******************
    //********Size********
    //*******************
    public function size()
    {
        $sizes = CustomGlobal::getData('sizes');
        return view('backend.basicSetup.size',compact('sizes'));
    }


    public function saveSize(SizeRequest $request)
    {
        $saveSize = BasicSetupService::saveSize($request->all());

        if($saveSize === true){
            return redirect('size')->with('success','Size Save Successfully !');
        } else {
            return redirect('size')->with('fail','Something Went Wrong !');
        }
    }








}
