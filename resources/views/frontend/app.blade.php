<!DOCTYPE html>
<html lang="en">

	@include('frontend._partial._head')

	<body class="cnt-home">

		@include('frontend._partial._header')

			@section('breadcrumb')
			@show

			<div class="body-content outer-top-xs" id="top-banner-and-menu">
	        	<div class="container">
	        			        			
	        			@section('main-content')
	                    @show	        		

	        		@include('frontend._partial._bottom-carousel')

				</div>
			</div>

		@include('frontend._partial._footer')

		@include('frontend._partial._script')	

	</body>
</html>