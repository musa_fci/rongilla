@if(session()->has('fail'))
<p class="alert alert-danger" style="text-align: center;">
  {{ session()->get('fail') }}
</p>
@endif
