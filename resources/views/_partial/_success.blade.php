@if(session()->has('success'))
<p class="alert alert-success" style="text-align: center;">
  {{ session()->get('success') }}
</p>
@endif
