@extends('seller.app')

@section('headerTitle','Add Product')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">

            @include('_partial._success')
            @include('_partial._fail')
            @include('_partial._error')

          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">Add Product</h4>
                  </div>

                  <div class="content">

                      <form action="{{URL::to('product')}}" method="post" enctype="multipart/form-data" >
                        <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                  <label>Category</label>
                                  <select name="fk_category_id" class="form-control" id="category">
                                    <option value="0">-- select --</option>
                                    @if(count($categories))
                                      @foreach($categories as $category)
                                      <option value="{{ $category->id }}">
                                        {{ $category->category_name_en }} / {{ $category->category_name_bn }}
                                      </option>
                                      @endforeach
                                    @endif                                   
                                  </select>
                              </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group">
                                <label>Sub-Category</label>
                                <select class="form-control" name="fk_sub_category_id" id="subcategory">
                                  <option value="0">-- Select --</option>                                    
                                </select>
                              </div>                              
                          </div>

                          <div class="col-md-4">
                            <div class="form-group">
                                <label>Sub-Sub-Category</label>
                                <select class="form-control" name="fk_sub_sub_category_id" id="subsubcategory">
                                  <option value="0">-- Select --</option>                                    
                                </select>
                              </div>                              
                          </div>

                        </div>
                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Name (English)</label>                                  
                                  <input type="text" name="product_name_en" value="{{old('product_name_en')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Name (Bangla)</label>
                                  <input type="text" name="product_name_bn" value="{{old('product_name_bn')}}" class="form-control">
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Slug (like-as-this-style)</label>
                                  <input type="text" name="slug" value="{{old('slug')}}" class="form-control">
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Details English (Separate By #)</label>
                                  <textarea name="details_en" value="{{old('details_en')}}" class="form-control" id="" cols="" rows="5"></textarea>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Details Bangla (Separate By #)</label>
                                  <textarea name="details_bn" value="{{old('details_bn')}}" class="form-control" id="" cols="" rows="5"></textarea>
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Video-Youtube Link (Optional)</label>
                                  <input type="text" name="product_video" value="{{old('product_video')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Quantity</label>
                                  <input type="text" name="quantity" value="{{old('quantity')}}" class="form-control">
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Product Type</label>
                                  <select name="product_type" class="form-control" id="">
                                    <option value="0">-- select --</option>
                                    <option value="new">New</option>
                                    <option value="hot">Hot</option>
                                    <option value="sell">Sell</option>
                                  </select>
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label>Product Price</label>
                                  <input type="text" name="price" value="{{old('price')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label>Product Discount (Optional)</label>
                                  <input type="text" name="discount" value="{{old('discount')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label>Commission (Min 5)</label>
                                  <input type="text" name="commission" value="{{old('commission')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label>Delivery In</label>
                                  <select name="placement_type" class="form-control" id="">
                                    <option value="0">-- select --</option>
                                    <option value="1">Dhaka</option>
                                    <option value="2">Outside of Dhaka</option>                                    
                                  </select>
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Product Colors</label>
                              <select multiple data-title="Select Multiple Color" name="color_id[]" class="selectpicker" data-style="btn-info btn-fill btn-block" data-menu-style="dropdown-blue">
                                @if(count($colors))
                                  @foreach($colors as $color)
                                  <option value="{{ $color->id }}" style="background:{{ $color->color_code }};color:#FFF;">
                                    {{ $color->color_name_en }}
                                  </option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Product Sizes</label>
                              <select multiple data-title="Select Multiple Size" name="size_id[]" class="selectpicker" data-style="btn-info btn-fill btn-block" data-menu-style="dropdown-blue">
                                @if(count($sizes))
                                  @foreach($sizes as $size)
                                  <option value="{{ $size->id }}">
                                    {{ $size->size_name_en }}
                                  </option>
                                  @endforeach
                                @endif
                              </select>
                            </div>                            
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Add Product Tag</label>
                              <select multiple data-title="Select Multiple Tag" name="tag_id[]" class="selectpicker" data-style="btn-info btn-fill btn-block" data-menu-style="dropdown-blue">
                                @if(count($tags))
                                  @foreach($tags as $tag)
                                  <option value="{{ $tag->id }}">
                                    {{ $tag->tag_name_en }}
                                  </option>
                                  @endforeach
                                @endif
                              </select>
                            </div>                            
                          </div>
                          
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Product Image (700 x 700)</label>
                              <input type="file" name="image[]" multiple="" class="form-control">                              
                            </div>                            
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Product Refund Policy</label>
                                  <textarea name="refund_policy" value="{{old('refund_policy')}}" class="form-control" id="" cols="" rows="5"></textarea>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Comment</label>
                                  <textarea name="comment" value="{{old('comment')}}" class="form-control" id="" cols="" rows="5"></textarea>
                              </div>
                          </div>
                        </div>                        

                          <button type="submit" class="btn btn-info btn-fill pull-left">Add Product</button>
                          <div class="clearfix"></div>
                      </form> 

                  </div>
                  
              </div>
          </div>

      </div>
  </div>
</div>

@endsection



@section('script')
  <script type="text/javascript">

    $('#category').on('change',function(e){
      var cat_id = e.target.value;
      $.ajax({
        url: "{{url('getSubCategory')}}/"+cat_id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
          if(data.length > 0){
            var subcat = $('#subcategory').empty();
            subcat.append('<option selected disabled>Select Sub-Category</option>');
            $.each(data,function(key,val){
              subcat.append('<option value ="'+val.id+'">'+val.sub_category_name_en+'</option>');
            });
          }
        }
      });
    });

    $('#subcategory').on('change',function(e){
      var sub_cat_id = e.target.value;
      $.ajax({
        url: "{{url('getSubSubCategory')}}/"+sub_cat_id,
        type: "GET",
        dataType: "JSON",
        success: function(data){          
          if(data.length > 0){
            var subsubcat = $('#subsubcategory').empty();
            subsubcat.append('<option selected disabled>Select Sub-Sub-Category</option>');
            $.each(data,function(key,val){
              subsubcat.append('<option value ="'+val.id+'">'+val.sub_sub_category_name_en+'</option>');
            });
          }
        }
      });
    });


      $(document).ready(function() {
          $('#datatables').DataTable({
              "pagingType": "full_numbers",
              "lengthMenu": [
                  [10, 25, 50, -1],
                  [10, 25, 50, "All"]
              ],
              responsive: true,
              language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Search records",
              }

          });

          var table = $('#datatables').DataTable();

          // Edit record
          // table.on('click', '.edit', function() {
          //     $tr = $(this).closest('tr');

          //     var data = table.row($tr).data();
          //     alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
          // });

          // Delete a record
          table.on('click', '.remove', function(e) {
              $tr = $(this).closest('tr');
              table.row($tr).remove().draw();
              e.preventDefault();
          });

          //Like record
          // table.on('click', '.like', function() {
          //     alert('You clicked on Like button');
          // });
      });
  </script>
@endsection