@extends('seller.app')

@section('headerTitle','Profile')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-8">
              <div class="card">
                  <div class="header">
                      <h4 class="title">Update Your Account Information</h4>
                  </div>

                  <div class="content">
                      <form action="{{URL::to('SellerUpdate')}}" method="post" >
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label>National ID</label>
                                      <input type="text" class="form-control" name="nid" disabled="disabled" value="{{Session()->get('SellerAdmin.nid')}}">
                                  </div>
                              </div>
                          </div> 

                          <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>Name</label>
                                      <input type="hidden" value="{{csrf_token()}}" name="_token">
                                      <input type="text" class="form-control" name="name" value="{{Session()->get('SellerAdmin.name')}}">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Email address</label>
                                      <input type="email" class="form-control" name="email" value="{{Session()->get('SellerAdmin.email')}}">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>Mobile</label>
                                      <input type="text" class="form-control" name="mobile" value="{{Session()->get('SellerAdmin.mobile')}}">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>Payment Method</label>
                                      <input type="text" class="form-control" name="payment_method" value="{{Session()->get('SellerAdmin.payment_method')}}">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>ACC. Holder Name</label>
                                      <input type="text" class="form-control" name="ac_holder_name" value="{{Session()->get('SellerAdmin.ac_holder_name')}}">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>Account Number</label>
                                      <input type="text" class="form-control" name="ac_number" value="{{Session()->get('SellerAdmin.ac_number')}}">
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label>Business Name</label>
                                      <input type="text" class="form-control" name="business_name" value="{{Session()->get('SellerAdmin.business_name')}}">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label>Business Type</label>
                                      <input type="text" class="form-control" name="business_type" value="{{Session()->get('SellerAdmin.business_type')}}">
                                  </div>
                              </div>
                          </div>                        

                          <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>Website</label>
                                      <input type="text" class="form-control" name="website" value="{{Session()->get('SellerAdmin.website')}}">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>District</label>
                                      <input type="text" class="form-control" name="district" value="{{Session()->get('SellerAdmin.district')}}">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label>Account Create</label>
                                      <input type="text" class="form-control" name="created_at" disabled="disabled" value="{{Session()->get('SellerAdmin.created_at')}}">
                                  </div>
                              </div>
                          </div>                          

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Address</label>
                                      <input type="text" class="form-control" name="address" value="{{Session()->get('SellerAdmin.address')}}">
                                  </div>
                              </div>
                          </div>                          

                          <button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>
                          <div class="clearfix"></div>
                      </form>                    
                  </div>
                  
              </div>
          </div>
          <div class="col-md-4">
              <div class="card card-user">
                  <div class="image">
                      <img src="{{URL::to('seller/assets/img/full-screen-image-3.jpg')}}" alt="..."/>
                  </div>
                  <div class="content">
                      <div class="author">
                           <a href="#">
                          <img class="avatar border-gray" src="{{URL::to('seller/assets/img/default-avatar.png')}}" alt="..."/>

                            <h4 class="title">{{Session()->get('SellerAdmin.name')}}<br />
                               <small>{{Session()->get('SellerAdmin.business_name')}}</small>
                            </h4>
                          </a>
                      </div>                      
                  </div>
                  <hr>
                  <div class="text-center">
                      <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                      <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                      <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>

                  </div>
              </div>
          </div>

      </div>
  </div>
</div>

@endsection
