<!doctype html>
<html lang="en">
    @include('seller._partial._head')
    <body>

        <div class="wrapper">
            @include('seller._partial._nav')

            <div class="main-panel">
                @include('seller._partial._headerNav')

                    @section('content')
                    @show

                 @include('seller._partial._footer')
            </div>

        </div>

    @include('seller._partial._script')
    
    </body>
</html>
