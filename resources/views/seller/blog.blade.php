@extends('seller.app')

@section('headerTitle','Blog')

@section('content')


<div class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
          <div class="card">

              <div class="content content-full-width">

                  <ul role="tablist" class="nav nav-tabs">
                      <li role="presentation" class="{{ !isset($_GET['page']) ? 'active' : '' }}">
                          <a href="#add-blog" data-toggle="tab"><i class="fa fa-info"></i> Add Blog</a>
                      </li>
                      <li class="{{ isset($_GET['page']) ? 'active' : '' }}">
                          <a href="#blog-list" data-toggle="tab"><i class="fa fa-user"></i> Blog List</a>
                      </li>
                  </ul>

                  <div class="tab-content">
                      <div id="add-blog" class="tab-pane {{ !isset($_GET['page']) ? 'active' : '' }}">
                        <div class="card">
                            @include('_partial._success')
                            @include('_partial._fail')
                            @include('_partial._error')
                            <div class="content">
                                <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Category</label>
                                            <div class="col-sm-10">
                                              <select class="form-control" name="category">
                                                  <option value="">Select Category</option>
                                                  <option value="sports">Sports</option>
                                                  <option value="travel">Travel</option>
                                                  <option value="technology">Technology</option>
                                                  <option value="hollywood">Hollywood</option>
                                              </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="title" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Blog Body</label>
                                            <div class="col-sm-10">
                                                <textarea name="body" class="form-control" rows="8"></textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Image</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="img" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <!-- <label class="col-sm-2 control-label"></label> -->
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <label class="checkbox">
                                                    <input type="checkbox" name="status" value="2" data-toggle="checkbox">I Want To Publish My Blog Now.
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                          <button type="submit" class="btn btn-fill btn-info">Submit</button>
                                        </div>
                                      </div>
                                    </fieldset>
                                 </form>
                            </div>
                        </div>  <!-- end card -->
                      </div>

                      <div id="blog-list" class="tab-pane {{ isset($_GET['page']) ? 'active' : '' }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="content table-responsive table-full-width">
                                        <table class="table table-bigboy">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Thumb</th>
                                                    <th>Blog Title</th>
                                                    <th class="th-description">Description</th>
                                                    <th class="text-right">Date</th>
                                                    <th class="text-right">Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              @if(isset($blogs))
                                                <?php $i = 0 ?>
                                              @foreach($blogs as $blog)
                                                <?php $i++ ?>
                                                <tr>
                                                    <td>
                                                        <div class="img-container">
                                                            <img src="{{URL::to('public/images').'/'.$blog->img}}" alt="...">
                                                        </div>
                                                    </td>
                                                    <td class="td-name">
                                                        {{ $blog->title }}
                                                    </td>
                                                    <td>
                                                        {{ substr($blog->body,0,80) }}
                                                    </td>
                                                    <td class="td-number">{{ $blog->created_at }}</td>
                                                    <td class="td-number">
                                                       @if($blog->status == '1')
                                                         <button class="btn btn-success">Publish</button>
                                                       @else
                                                         <button class="btn btn-danger">Unpublish</button>
                                                       @endif
                                                    </td>
                                                    <td class="td-actions">
                                                        <button type="button" data-toggle="modal" data-target="#myModal-{{ $i }}" rel="tooltip" data-placement="left" title="View Post" class="btn btn-info btn-simple btn-icon">
                                                            <i class="fa fa-image"></i>
                                                        </button>
                                                        <!--Blog View Modal -->
                                                				<div class="modal fade  bs-example-modal-lg" id="myModal-{{ $i }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                				  <div class="modal-dialog modal-lg" role="document">
                                                				    <div class="modal-content">
                                                				      <div class="modal-header">
                                                				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                				        <h4 class="modal-title" id="myModalLabel">{{ $blog->title }}</h4>
                                                				      </div>
                                                				      <div class="modal-body">
                                                                <img src="{{URL::to('public/images').'/'.$blog->img}}" alt="..." style="width:100%;height:100%;margin-bottom:30px;">
                                                				        {{ $blog->body }}
                                                				      </div>
                                                				      <div class="modal-footer">
                                                				        <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                                                				        <button type="button" class="btn btn-success btn-fill">Save changes</button>
                                                				      </div>
                                                				    </div>
                                                				  </div>
                                                				</div>

                                                        <a href="{{URL::to('blog/'.$blog->id.'/edit')}}"  data-toggle="modal" data-target="#editBlog-{{ $i }}" type="button" rel="tooltip" data-placement="left" title="Edit Post" class="btn btn-success btn-simple btn-icon">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <!--Blog Edit Modal -->
                                                        <div class="modal fade bs-example-modal-lg" id="editBlog-{{ $i }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                				  <div class="modal-dialog modal-lg" role="document">
                                                				    <div class="modal-content">
                                                				      <div class="modal-header">
                                                				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                				        <h4 class="modal-title" id="myModalLabel">Blog Edit</h4>
                                                				      </div>
                                                				      <div class="modal-body">
                                                                <div class="card">
                                                                    <div class="content">
                                                                        <form method="post" action="{{URL::to('blog/'.$blog->id.'/edit')}}" class="form-horizontal" enctype="multipart/form-data">
                                                                            <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                                                            <fieldset>
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-2 control-label">Category</label>
                                                                                    <div class="col-sm-10">
                                                                                      <select class="form-control" name="category">
                                                                                          <option value="">Select Category</option>
                                                                                          <?php
                                                                                          if(isset($cats)){
                                                                                            foreach($cats as $cat){
                                                                                              ?>
                                                                                              <option value="<?php echo $cat->category; ?>"<?php if($blog->category == $cat->category){ ?> selected="selected" <?php } ?>><?php echo ucfirst($cat->category); ?></option>
                                                                                              <?php
                                                                                            }
                                                                                          }
                                                                                          ?>
                                                                                      </select>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset>
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-2 control-label">Title</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input type="text" name="title" value="{{ $blog->title }}" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset>
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-2 control-label">Blog Body</label>
                                                                                    <div class="col-sm-10">
                                                                                        <textarea name="body" class="form-control" rows="8">{{ $blog->body }}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset>
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-2 control-label">Image</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input type="file" name="img" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset>
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-10 col-sm-offset-2">
                                                                                        <label class="checkbox">
                                                                                            <input type="checkbox" name="status" {{ $blog->status == "1" ? "checked" : "Goodbye" }} value="1" data-toggle="checkbox">I Want To Publish My Blog Now.
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset>
                                                                              <div class="form-group">
                                                                                <label class="col-sm-2 control-label"></label>
                                                                                <div class="col-sm-10">
                                                                                  <button type="submit" class="btn btn-fill btn-info">Submit</button>
                                                                                </div>
                                                                              </div>
                                                                            </fieldset>
                                                                         </form>
                                                                    </div>
                                                                </div>  <!-- end card -->
                                                				      </div>
                                                				      <div class="modal-footer">
                                                				        <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                                                				      </div>
                                                				    </div>
                                                				  </div>
                                                				</div>

                                                        <a href="{{URL::to('blog/'.$blog->id.'/delete')}}" type="button" rel="tooltip" data-placement="left" title="Remove Post" class="btn btn-danger btn-simple btn-icon ">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        {{ $blogs->links() }}
                                    </div>
                                </div><!--  end card  -->
                            </div> <!-- end col-md-12 -->
                        </div> <!-- end row -->
                      </div>
                  </div>

              </div>
          </div>
      </div>
    </div>

  </div>
</div>
@endsection
