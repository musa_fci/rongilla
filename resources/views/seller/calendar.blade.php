@extends('seller.app')

@section('headerTitle','Calendar')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="card card-calendar">
                    <div class="content">
                        <div id="fullCalendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $().ready(function(){
        demo.initFullCalendar();
    });
</script>
@endsection
