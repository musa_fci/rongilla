<!doctype html>
<html lang="en">
@include('seller._partial._head')

<body>

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{URL::to('SellerAdmin')}}">
                        Looking to login?
                    </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper wrapper-full-page">
        <div class="full-page register-page" data-color="azure" data-image="{{URL::to('seller/assets/img/full-screen-image-3.jpg')}}">

            <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="header-text">
                                <h2>www.rongilla.com</h2>
                                <h4>Register for free and experience the dashboard today</h4>
                                <hr />
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-2">
                            <div class="media">
                                <div class="media-left">
                                    <div class="icon">
                                        <i class="pe-7s-user"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4>Free Account</h4> Here you can write a feature description for your dashboard, let the users know what is the value that you give them.
                                </div>
                            </div>

                            <div class="media">
                                <div class="media-left">
                                    <div class="icon">
                                        <i class="pe-7s-graph1"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4>Awesome Performances</h4> Here you can write a feature description for your dashboard, let the users know what is the value that you give them.

                                </div>
                            </div>

                            <div class="media">
                                <div class="media-left">
                                    <div class="icon">
                                        <i class="pe-7s-headphones"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4>Global Support</h4> Here you can write a feature description for your dashboard, let the users know what is the value that you give them.

                                </div>
                            </div>

                        </div>

                        <div class="col-md-4 col-md-offset-s1">

                            @include('_partial._error') 
                            @include('_partial._success')

                            <form method="post" action="{{URL::to('seller-registration')}}">
                                <div class="card card-plain">
                                    <div class="content">
                                        <div class="form-group">
                                            <input type="hidden" value="{{csrf_token()}}" name="_token">
                                            <input type="text" name="name" value="{{old('name')}}" placeholder="Your Name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{old('email')}}" placeholder="Your Email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="Your Password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" placeholder="Password Confirmation" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="mobile" value="{{old('mobile')}}" placeholder="Your Mobile No" class="form-control">
                                        </div>                                        
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-fill btn-neutral btn-wd">Create Rongilla Account</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer footer-transparent">
                <div class="container">
                    <p class="copyright text-center">
                        &copy; 2016 <a href="musa">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>

        </div>

    </div>

</body>

@include('seller._partial._script')

<script type="text/javascript">
    $().ready(function() {
        lbd.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 1000)
    });
</script>

</html>