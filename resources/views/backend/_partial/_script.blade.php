
<!-- <script>
  var url = window.location;
  $("li div ul li a[href^='"+url+"']").parent().parent().parent().prev().attr('aria-expanded',true);
  $("li div ul li a[href^='"+url+"']").parent().parent().parent().prev().attr('class','');
  $("li div ul li a[href^='"+url+"']").parent().parent().parent().addClass('in');
</script> -->



<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->

{!! HTML::script('backend/assets/js/jquery-ui.min.js') !!}
{!! HTML::script('backend/assets/js/bootstrap.min.js') !!}

{!! HTML::script('backend/') !!}

<!--  Forms Validations Plugin -->
{!! HTML::script('backend/assets/js/jquery.validate.min.js') !!}

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
{!! HTML::script('backend/assets/js/moment.min.js') !!}

<!--  Date Time Picker Plugin is included in this js file -->
{!! HTML::script('backend/assets/js/bootstrap-datetimepicker.js') !!}

<!--  Select Picker Plugin -->
{!! HTML::script('backend/assets/js/bootstrap-selectpicker.js') !!}

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
{!! HTML::script('backend/assets/js/bootstrap-checkbox-radio-switch-tags.js') !!}

<!--  Charts Plugin -->
{!! HTML::script('backend/assets/js/chartist.min.js') !!}

<!--  Notifications Plugin    -->
{!! HTML::script('backend/assets/js/bootstrap-notify.js') !!}

<!-- Sweet Alert 2 plugin -->
{!! HTML::script('backend/assets/js/sweetalert2.js') !!}

<!-- Vector Map plugin -->
{!! HTML::script('backend/assets/js/jquery-jvectormap.js') !!}

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
{!! HTML::script('backend/assets/js/jquery.bootstrap.wizard.min.js') !!}

<!--  Bootstrap Table Plugin    -->
{!! HTML::script('backend/assets/js/bootstrap-table.js') !!}

<!--  Plugin for DataTables.net  -->
{!! HTML::script('backend/assets/js/jquery.datatables.js') !!}

<!--  Full Calendar Plugin    -->
{!! HTML::script('backend/assets/js/fullcalendar.min.js') !!}

<!-- Light Bootstrap Dashboard Core javascript and methods -->
{!! HTML::script('backend/assets/js/light-bootstrap-dashboard.js') !!}
{!! HTML::script('backend/assets/js/demo.js') !!}


@section('script')
@show
