<!doctype html>
<html lang="en">
    @include('backend._partial._head')
    <body>

        <div class="wrapper">
            @include('backend._partial._nav')

            <div class="main-panel">
                @include('backend._partial._headerNav')

                    @section('content')
                    @show

                 @include('backend._partial._footer')
            </div>

        </div>

    @include('backend._partial._script')
    
    </body>
</html>
