<!doctype html>
<html lang="en">
@include('backend._partial._head')
<body>

<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li>
                   <a href="login">
                        Looking to login?
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper wrapper-full-page">
    <div class="full-page register-page" data-color="red" data-image="{{URL::to('public/backend/assets/img/full-screen-image-3.jpg')}}">

    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="header-text">
                            <h2>Light Bootstrap Dashboard PRO</h2>
                            <h4>Register for free and experience the dashboard today</h4>
                            <hr />
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <div class="media">
                            <div class="media-left">
                                <div class="icon">
                                    <i class="pe-7s-user"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <h4>Free Account</h4>
                                Here you can write a feature description for your dashboard, let the users know what is the value that you give them.
                            </div>
                        </div>

                        <div class="media">
                            <div class="media-left">
                                <div class="icon">
                                    <i class="pe-7s-graph1"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <h4>Awesome Performances</h4>
                                Here you can write a feature description for your dashboard, let the users know what is the value that you give them.

                            </div>
                        </div>

                        <div class="media">
                            <div class="media-left">
                                <div class="icon">
                                    <i class="pe-7s-headphones"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <h4>Global Support</h4>
                                Here you can write a feature description for your dashboard, let the users know what is the value that you give them.

                            </div>
                        </div>

                    </div>

                    <div class="col-md-4 col-md-offset-s1">
                      @include('_partial._error')
                      @include('_partial._success')
                        <form method="post" action="{{route('register')}}">
                            <div class="card card-plain">
                                <div class="content">
                                    <div class="form-group">
																			<input type="hidden" value="{{csrf_token()}}" name="_token">
                                      <input type="text" name="name" value="{{old('name')}}" placeholder="Your Name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" value="{{old('email')}}" placeholder="Your Email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" placeholder="Your Password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" placeholder="Password Confirmation" class="form-control">
                                    </div>
																		<div class="form-group">
																				<textarea name="address" placeholder="Your Address" class="form-control">{{old('address')}}</textarea>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-neutral btn-wd">Create Free Account</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    	<footer class="footer footer-transparent">
            <div class="container">
                <p class="copyright text-center">
                    &copy; 2016 <a href="musa">Creative Tim</a>, made with love for a better web
                </p>
            </div>
        </footer>

    </div>

</div>


</body>

    @include('backend._partial._script')

    <script type="text/javascript">
        $().ready(function(){
            lbd.checkFullPageBackgroundImage();

            setTimeout(function(){
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 1000)
        });
    </script>

</html>
