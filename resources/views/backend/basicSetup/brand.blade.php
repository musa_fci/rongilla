@extends('backend.app')

@section('headerTitle','Brand')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">

            @include('_partial._success')
            @include('_partial._fail')
            @include('_partial._error')

          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">Add Brand</h4>
                  </div>

                  <div class="content">

                      <form action="{{URL::to('brand')}}" method="post" enctype="multipart/form-data" >
                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Brand Name (English)</label>
                                  <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                  <input type="text" name="brand_name_en" value="{{old('brand_name_en')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Brand Name (Bangla)</label>
                                  <input type="text" name="brand_name_bn" value="{{old('brand_name_bn')}}" class="form-control">
                              </div>
                          </div>
                        </div>

                        <div class="row">                          
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Brand Link</label>
                                  <input type="text" name="brand_link" value="{{old('brand_link')}}" class="form-control">
                              </div>
                          </div>                                            
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Upload (Feature Image)</label>
                                  <input type="file" name="feature_img" class="form-control">
                              </div>
                          </div>
                        </div>

                          <button type="submit" class="btn btn-info btn-fill pull-left">Add Brand</button>
                          <div class="clearfix"></div>
                      </form> 

                  </div>
                  
              </div>
          </div>


          <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="fresh-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name (EN)</th>
                                        <th>Name (BN)</th>
                                        <th>Brand Link</th>
                                        <th>Feature</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                  @if(count($brands))
                                    <?php $i=0; ?>
                                    @foreach($brands as $brand)
                                      <?php $i++; ?>
                                      <tr>
                                          <td>{{ $i }}</td>
                                          <td>{{ $brand->brand_name_en }}</td>
                                          <td>{{ $brand->brand_name_bn }}</td>                                          
                                          <td>{{ $brand->brand_link }}</td>                                          
                                          <td>
                                            <a href="" data-toggle="modal" data-target="#myModal-{{ $brand->id }}">
                                              <img src="{{URL::to('images/brand/feature').'/'.$brand->feature_img}}" alt="" width="30" height="30">
                                            </a>
                                            <div class="modal fade bs-example-modal-xs" id="myModal-{{ $brand->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog modal-xs" role="document">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body">
                                                      <img src="{{URL::to('images/brand/feature').'/'.$brand->feature_img}}" alt="" style="padding-left: 35px;">
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                          </td>
                                          
                                          <td class="text-left">    
                                              {{-- Category Edit --}}
                                              <a href="#" data-toggle="modal" data-target="#editCategory-{{ $brand->id }}" class="btn btn-simple btn-warning btn-icon edit">
                                                <i class="fa fa-edit"></i>
                                              </a>
                                               <div class="modal fade" id="editCategory-{{ $brand->id }}">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>

                                                      <div class="modal-body">

                                                         <form action="{{URL::to('saveCategory')}}" method="post" enctype="multipart/form-data" >
                                                            <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="form-group">
                                                                      <label>Category Name (English)</label>
                                                                      <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                                                      <input type="text" name="category_name_en" value="{{old('category_name_en')}}" class="form-control">
                                                                  </div>
                                                              </div>
                                                            </div>

                                                            <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="form-group">
                                                                      <label>Category Name (Bangla)</label>
                                                                      <input type="text" name="category_name_bn" value="{{old('category_name_bn')}}" class="form-control">
                                                                  </div>
                                                              </div>
                                                            </div>

                                                            <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="form-group">
                                                                      <label>Upload (Icon)</label>
                                                                      <input type="file" name="icon" class="form-control">
                                                                  </div>
                                                              </div>
                                                            </div>

                                                            <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="form-group">
                                                                      <label>Upload (Feature Image)</label>
                                                                      <input type="file" name="feature_img" class="form-control">
                                                                  </div>
                                                              </div>
                                                            </div>

                                                            <div class="row">                        
                                                              <div class="col-md-12">
                                                                  <div class="form-group">
                                                                      <label>Is Selected</label>
                                                                      <select name="is_selected" class="form-control">
                                                                        <option value="0" selected="selected">Not Selected</option>
                                                                        <option value="1">Selected</option>
                                                                      </select>
                                                                  </div>
                                                              </div>
                                                            </div>

                                                              <button type="submit" class="btn btn-info btn-fill pull-left">Update Category</button>
                                                              <div class="clearfix"></div>
                                                          </form> 
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                                                        
                                                      </div>                                                      
                                                    </div>
                                                  </div>
                                                </div>
                                              {{-- Category Delete --}}
                                              <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                <i class="fa fa-times"></i>
                                              </a>
                                          </td>
                                      </tr>
                                    @endforeach
                                  @endif          
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
          </div>

      </div>
  </div>
</div>

@endsection



@section('script')
  <script type="text/javascript">
      $(document).ready(function() {
          $('#datatables').DataTable({
              "pagingType": "full_numbers",
              "lengthMenu": [
                  [10, 25, 50, -1],
                  [10, 25, 50, "All"]
              ],
              responsive: true,
              language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Search records",
              }

          });

          var table = $('#datatables').DataTable();

          // Edit record
          // table.on('click', '.edit', function() {
          //     $tr = $(this).closest('tr');

          //     var data = table.row($tr).data();
          //     alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
          // });

          // Delete a record
          table.on('click', '.remove', function(e) {
              $tr = $(this).closest('tr');
              table.row($tr).remove().draw();
              e.preventDefault();
          });

          //Like record
          // table.on('click', '.like', function() {
          //     alert('You clicked on Like button');
          // });
      });
  </script>
@endsection