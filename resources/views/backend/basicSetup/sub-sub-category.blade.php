@extends('backend.app')

@section('headerTitle','Sub-Category')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">

          @include('_partial._success')
          @include('_partial._fail')
          @include('_partial._error')

          <div class="col-md-12">
              <div class="card">
                  <div class="header">
                      <h4 class="title">Add Sub-Sub-Category</h4>
                  </div>

                  <div class="content">
                      <form action="{{URL::to('saveSubSubCategory')}}" method="post" enctype="multipart/form-data" >

                        <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Category Name</label>
                                  <select class="form-control" id="category" name="fk_category_id">
                                    <option value="0">-- Select --</option>
                                    @if(count($categories))
                                      @foreach($categories as $category)
                                        <option value="{{ $category->id }}">
                                          {{ $category->category_name_en }} / {{ $category->category_name_bn }}
                                        </option>
                                      @endforeach
                                    @endif
                                  </select>
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Sub-Category Name</label>
                                  <select class="form-control" id="subcategory" name="fk_sub_category_id">
                                    <option value="0">-- Select --</option>                                    
                                  </select>
                              </div>
                          </div>
                        </div>                        

                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Sub-Category Name (English)</label>
                                  <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                  <input type="text" name="sub_sub_category_name_en" value="{{old('sub_sub_category_name_en')}}" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label>Sub-Category Name (Bangla)</label>
                                  <input type="text" name="sub_sub_category_name_bn" value="{{old('sub_sub_category_name_bn')}}" class="form-control">
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Upload (Feature Image)</label>
                                  <input type="file" name="feature_img" class="form-control">
                              </div>
                          </div>
                        </div>                                                    

                          <button type="submit" class="btn btn-info btn-fill pull-left">Add Sub-Sub-Category</button>
                          <div class="clearfix"></div>
                      </form>                    
                  </div>

              </div>
          </div>

          <div class="col-md-12">
            <div class="card">
                <div class="content">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <div class="fresh-datatables">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Category Name</th>
                                    <th>Sub-Category Name</th>
                                    <th>Sub-Sub-Cat Name (EN)</th>
                                    <th>Sub-Sub-Cat Name (BN)</th>
                                    <th>Feature</th>
                                    <th class="disabled-sorting text-left">Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                              @if(count($subSubCategory))
                                <?php $i = 0; ?>
                                @foreach($subSubCategory as $subSubCategory)
                                  <?php $i++; ?>
                                  <tr>
                                      <td>{{ $i }}</td>
                                      <td>{{ $subSubCategory->category_name_en }}</td>
                                      <td>{{ $subSubCategory->sub_category_name_en }}</td>
                                      <td>{{ $subSubCategory->sub_sub_category_name_en }}</td>
                                      <td>{{ $subSubCategory->sub_sub_category_name_en }}</td>                                      
                                      <td>
                                        <a href="" data-toggle="modal" data-target="#myModal-{{ $subSubCategory->id }}">
                                          <img src="{{URL::to('images/subSubCategory/feature').'/'.$subSubCategory->feature_img}}" alt="" width="30" height="30">
                                        </a>
                                        <div class="modal fade bs-example-modal-xs" id="myModal-{{ $subSubCategory->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog modal-xs" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <img src="{{URL::to('images/subSubCategory/feature').'/'.$subSubCategory->feature_img}}" alt="" style="padding-left: 35px;">
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                      </td>                                      

                                      <td class="text-left">    
                                          {{-- Category Edit --}}
                                          <a href="#" data-toggle="modal" data-target="#editCategory-{{ $subSubCategory->id }}" class="btn btn-simple btn-warning btn-icon edit">
                                            <i class="fa fa-edit"></i>
                                          </a>
                                           <div class="modal fade" id="editCategory-{{ $subSubCategory->id }}">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>

                                                  <div class="modal-body">

                                                     <form action="" method="post" enctype="multipart/form-data" >
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                              <div class="form-group">
                                                                  <label>Category Name (English)</label>
                                                                  <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                                                  <input type="text" name="category_name_en" value="{{old('sub_category_name_en')}}" class="form-control">
                                                              </div>
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                          <div class="col-md-12">
                                                              <div class="form-group">
                                                                  <label>Category Name (Bangla)</label>
                                                                  <input type="text" name="category_name_bn" value="{{old('sub_category_name_bn')}}" class="form-control">
                                                              </div>
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                          <div class="col-md-12">
                                                              <div class="form-group">
                                                                  <label>Upload (Icon)</label>
                                                                  <input type="file" name="icon" class="form-control">
                                                              </div>
                                                          </div>
                                                        </div>

                                                        <div class="row">
                                                          <div class="col-md-12">
                                                              <div class="form-group">
                                                                  <label>Upload (Feature Image)</label>
                                                                  <input type="file" name="feature_img" class="form-control">
                                                              </div>
                                                          </div>
                                                        </div>

                                                        <div class="row">                        
                                                          <div class="col-md-12">
                                                              <div class="form-group">
                                                                  <label>Is Selected</label>
                                                                  <select name="is_selected" class="form-control">
                                                                    <option value="0" selected="selected">Not Selected</option>
                                                                    <option value="1">Selected</option>
                                                                  </select>
                                                              </div>
                                                          </div>
                                                        </div>

                                                          <button type="submit" class="btn btn-info btn-fill pull-left">Update Category</button>
                                                          <div class="clearfix"></div>
                                                      </form> 
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                                                        
                                                  </div>                                                      
                                                </div>
                                              </div>
                                            </div>
                                          {{-- Category Delete --}}
                                          <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                            <i class="fa fa-times"></i>
                                          </a>
                                      </td>
                                  </tr>
                                @endforeach
                              @endif          
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
          </div>

      </div>
  </div>
</div>

<script>
    $('#category').on('change',function(e){
        var cat_id = e.target.value;
        $.ajax({
            url: "{{url('getSubCategory')}}/"+cat_id,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                if(data.length > 0){
                    var subcat =  $('#subcategory').empty();
                    subcat.append('<option selected disabled>Select Sub-Category</option>');
                    $.each(data,function(key,val){
                        subcat.append('<option value ="'+val.id+'">'+val.sub_category_name_en+'</option>');
                    });
                }
                
            }
        });
    });
</script>

@endsection
