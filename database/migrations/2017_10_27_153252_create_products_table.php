<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_seller_id')->unsigned();
            $table->integer('fk_category_id')->unsigned();
            $table->integer('fk_sub_category_id')->unsigned();
            $table->integer('fk_sub_sub_category_id')->unsigned();
            $table->string('product_name_en');
            $table->string('product_name_bn');
            $table->string('slug');
            $table->string('product_code');
            $table->text('details_en');
            $table->text('details_bn');
            $table->string('product_video')->nullable();
            $table->string('quantity')->unsigned();
            $table->string('product_type');
            $table->string('price');
            $table->string('discount')->nullable();
            $table->string('price_after_discount')->nullable();
            $table->string('commission');
            $table->string('placement_type');            
            $table->text('refund_policy');
            $table->text('comment');
            $table->string('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
