<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductWiseSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_wise_size', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_seller_id')->unsigned();
            $table->integer('fk_product_id')->unsigned();
            $table->integer('fk_size_id')->unsigned();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_wise_size');
    }
}
