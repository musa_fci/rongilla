<?php

Route::get('/session', function () {
	dd(session()->all());	
});


Route::get('/','FrontendController@index');
// Route::get('detail','FrontendController@detail');
Route::get('detail/{id}/{slug}','FrontendController@singleProductDetail');


Route::get('sign-in','ClientController@login');
Route::post('sign-in','ClientController@checkLogin');
Route::post('client-reg','ClientController@registration');
Route::get('client-logout','ClientController@clientLogout');


//seller email vefiry
Route::get('afterSellerVerify/{email?}/{verify_token?}','SellerController@afterSellerVerify')->name('afterSellerVerify');