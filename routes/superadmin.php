<?php


/*******************************/
/*********Backend Start*********/
/*******************************/



Route::get('SuperAdmin','SuperAdminController@login');
Route::post('SuperAdmin','SuperAdminController@checkLogin');
Route::get('logout','SuperAdminController@logout');


Route::middleware(['SuperAdmin'])->group(function(){

    Route::get('admin','SuperAdminController@index');

    //*******************
    //*******Category****
    //*******************
    Route::get('category','BasicsetupController@category');
    Route::post('saveCategory','BasicsetupController@saveCategory');
    Route::get('category/{id}/delete','BasicsetupController@deleteCategory');
    Route::get('category/{id}/edit','BasicsetupController@editCategory');



    //*******************
    //***Sub-Category****
    //*******************
    Route::get('sub-category','BasicsetupController@subCategory');
    Route::post('saveSubCategory','BasicsetupController@saveSubCategory');



    //*******************
    //*Sub-Sub-Category**
    //*******************
    Route::get('getSubCategory/{categoryId}','BasicsetupController@getSubCategory');
    Route::get('sub-sub-category','BasicsetupController@subSubCategory');
    Route::post('saveSubSubCategory','BasicsetupController@saveSubSubCategory');


    //*******************
    //*******Brands******
    //*******************
    Route::get('brand','BasicsetupController@brand');
    Route::post('brand','BasicsetupController@saveBrand');


    //*******************
    //*******Tags********
    //*******************
    Route::get('tag','BasicsetupController@tag');
    Route::post('tag','BasicsetupController@saveTag');

    //*******************
    //*******Colors******
    //*******************
    Route::get('color','BasicsetupController@color');
    Route::post('color','BasicsetupController@saveColor');


    //*******************
    //*******Sizes*******
    //*******************
    Route::get('size','BasicsetupController@size');
    Route::post('size','BasicsetupController@saveSize');













    Route::get('admin/charts', function () {
        return view('backend.charts');
    });

    Route::get('admin/calendar', function () {
        return view('backend.calendar');
    });

    //components
    Route::get('admin/buttons',function(){
      return view('backend.components.buttons');
    });
    Route::get('admin/grid',function(){
      return view('backend.components.grid');
    });
    Route::get('admin/icons',function(){
      return view('backend.components.icons');
    });
    Route::get('admin/notifications',function(){
      return view('backend.components.notifications');
    });
    Route::get('admin/panels',function(){
      return view('backend.components.panels');
    });
    Route::get('admin/sweet-alert',function(){
      return view('backend.components.sweet-alert');
    });
    Route::get('admin/typography',function(){
      return view('backend.components.typography');
    });

    //forms
    Route::get('admin/form-regular',function(){
      return view('backend.forms.regular');
    });
    Route::get('admin/form-extended',function(){
      return view('backend.forms.extended');
    });
    Route::get('admin/form-validation',function(){
      return view('backend.forms.validation');
    });
    Route::get('admin/form-wizard',function(){
      return view('backend.forms.wizard');
    });


    //tables
    Route::get('admin/table-regular',function(){
      return view('backend.tables.regular');
    });
    Route::get('admin/table-extended',function(){
      return view('backend.tables.extended');
    });
    Route::get('admin/table-bootstrap',function(){
      return view('backend.tables.bootstrap-table');
    });
    Route::get('admin/table-datatables',function(){
      return view('backend.tables.datatables');
    });

    //maps
    Route::get('admin/google-map',function(){
      return view('backend.maps.google');
    });
    Route::get('admin/vector-map',function(){
      return view('backend.maps.vector');
    });
    Route::get('admin/fullscreen-map',function(){
      return view('backend.maps.fullscreen');
    });

    //Other pages
    Route::get('admin/user',function(){
      return view('backend.pages.user');
    });


});
