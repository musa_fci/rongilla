<?php

/*******************************/
/*********seller Start**********/
/*******************************/

Route::get('seller-registration','SellerController@registraView');
Route::post('seller-registration','SellerController@registration');

Route::get('SellerAdmin','SellerController@sellerLogin');
Route::post('SellerAdmin','SellerController@checkSellerLogin');
Route::get('sellerLogout','SellerController@sellerLogout');


Route::middleware(['SellerAdmin'])->group(function(){

    Route::get('seller-dashboard', function () {
        return view('seller.index');
    });

    //SellerAdmin
    Route::get('seller/user',function(){
      return view('seller.user');
    });

    Route::get('seller/user-update',function(){
      return view('seller.user-update');
    });

    Route::post('SellerUpdate','SellerController@SellerUpdate');

      //*******************
     //*******Products****
    //*******************    
    Route::get('getSubCategory/{categoryId}','ProductController@getSubCategory');
    Route::get('getSubSubCategory/{subCategoryId}','ProductController@getSubSubCategory');
    Route::get('product','ProductController@product');
    Route::post('product','ProductController@saveProduct');
















    Route::get('seller/charts', function () {
        return view('seller.charts');
    });

    Route::get('seller/calendar', function () {
        return view('seller.calendar');
    });

    //components
    Route::get('seller/buttons',function(){
      return view('seller.components.buttons');
    });
    Route::get('seller/grid',function(){
      return view('seller.components.grid');
    });
    Route::get('seller/icons',function(){
      return view('seller.components.icons');
    });
    Route::get('seller/notifications',function(){
      return view('seller.components.notifications');
    });
    Route::get('seller/panels',function(){
      return view('seller.components.panels');
    });
    Route::get('seller/sweet-alert',function(){
      return view('seller.components.sweet-alert');
    });
    Route::get('seller/typography',function(){
      return view('seller.components.typography');
    });

    //forms
    Route::get('seller/form-regular',function(){
      return view('seller.forms.regular');
    });
    Route::get('seller/form-extended',function(){
      return view('seller.forms.extended');
    });
    Route::get('seller/form-validation',function(){
      return view('seller.forms.validation');
    });
    Route::get('seller/form-wizard',function(){
      return view('seller.forms.wizard');
    });


    //tables
    Route::get('seller/table-regular',function(){
      return view('seller.tables.regular');
    });
    Route::get('seller/table-extended',function(){
      return view('seller.tables.extended');
    });
    Route::get('seller/table-bootstrap',function(){
      return view('seller.tables.bootstrap-table');
    });
    Route::get('seller/table-datatables',function(){
      return view('seller.tables.datatables');
    });

    //maps
    Route::get('seller/google-map',function(){
      return view('seller.maps.google');
    });
    Route::get('seller/vector-map',function(){
      return view('seller.maps.vector');
    });
    Route::get('seller/fullscreen-map',function(){
      return view('seller.maps.fullscreen');
    });


});
